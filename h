[33mcommit 879de6e6e17deb25dbe1e9163c90cd0ef0d7f3d5[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: Toni Cumplido <toni.cumplido@uab.cat>
Date:   Fri May 15 18:24:09 2020 +0200

    Index anterior con row y col, elijo col-md-4 para colapsar en resoluciones inferiores a iPad

[33mcommit 8956bc0993572af4ef660a6b42fd4d7f3e731c16[m[33m ([m[1;31morigin/master[m[33m)[m
Author: Toni Cumplido <toni.cumplido@uab.cat>
Date:   Fri May 15 18:12:39 2020 +0200

    Index anterior añadiendo container con 3 parrafos, definiendo titulos y descripciones

[33mcommit c9569a4f52d38b61c9f64ad9b0f9ab76a40d5e28[m
Author: Toni Cumplido <toni.cumplido@uab.cat>
Date:   Fri May 15 18:07:16 2020 +0200

    Index con jumbotron y titulo Hoteles

[33mcommit be3627d8e0c0c56f617524bd228a7bb9dfe9d21d[m
Author: Toni Cumplido <toni.cumplido@uab.cat>
Date:   Thu May 14 22:37:20 2020 +0200

    Incluimos Bootstrap

[33mcommit 2ad014c4724d506531ec316ffb82122f881fda5b[m
Author: Toni Cumplido <toni.cumplido@uab.cat>
Date:   Thu May 14 20:06:19 2020 +0200

    Pagina de inicio

[33mcommit 20c43053de1f513c66fc8586f55217c4abe6612c[m
Author: Toni Cumplido <toni.cumplido@uab.cat>
Date:   Thu May 14 19:44:10 2020 +0200

    Incluimos lite-server

[33mcommit 6fbbfc007148d50e2dc4b523bd1e24f73fe69d39[m
Author: Toni Cumplido <toni.cumplido@uab.cat>
Date:   Thu May 14 18:57:23 2020 +0200

    Definición del package.json

[33mcommit a8cf9d9e02d2a47776e20e50a89ef9108039d032[m
Author: Toni Cumplido <toni.cumplido@uab.cat>
Date:   Thu May 14 12:42:08 2020 +0200

    Primer commit del archivo README

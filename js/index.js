
$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 2000,
      pause: "hover"
    });

    $('#contacto').on('show.bs.modal', function (e){
        console.log('El modal contacto se está mostrando.');
        
        $(e.relatedTarget.children[0]).removeClass('text-muted');
        $(e.relatedTarget.children[0]).removeClass('oi-envelope-closed');
        $(e.relatedTarget.children[0]).addClass('oi-envelope-open');
        $(e.relatedTarget.children[0]).addClass('text-primary');
        
        $(e.relatedTarget).removeAttr('data-target');

        $('#exampleModalLabel')[0].innerHTML = 'Contacto con el hotel : <b>' + e.relatedTarget.parentElement.parentElement.children[0].innerHTML + '</b>';

        quien_abrio=e.relatedTarget;
    });
    $('#contacto').on('shown.bs.modal', function (e){
        console.log('El modal contacto se mostró.');
    });
    $('#contacto').on('hide.bs.modal', function (e){
        console.log('El modal contacto se está ocultando.');
    });
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('El modal contacto se ocultó.');

        
        $(quien_abrio.children[0]).removeClass('oi-envelope-open');
        $(quien_abrio.children[0]).addClass('oi-envelope-closed');
        $(quien_abrio.children[0]).addClass('text-muted');
        
        $('#exampleModalLabel')[0].innerHTML = 'Contacto con el hotel';

        $(quien_abrio).attr("data-target","#contacto");
    });

});